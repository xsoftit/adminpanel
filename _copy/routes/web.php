Route::middleware(['auth'])->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard')->middleware('role:admin');
});

