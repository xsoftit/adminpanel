(function () {
    "use strict";

    var treeviewMenu = $('.app-menu');

    // Toggle Sidebar
    $('[data-toggle="sidebar"]').click(function (event) {
        event.preventDefault();
        $('.app').toggleClass('sidenav-toggled');
    });

    // Activate sidebar treeview toggle
    $("[data-toggle='treeview']").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass('is-expanded')) {
            treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
        }
        $(this).parent().toggleClass('is-expanded');
    });

    // Set initial active toggle
    $("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

    //Activate bootstrip tooltips
    $("[data-toggle='tooltip']").tooltip();
})();

$(document).ready(function () {
    let timeout = null;
    $(document).ajaxStart(function () {
        $('button').prop('disabled', true);
        $('body').css('pointer-events', 'none');
        timeout = setTimeout(function () {
            let mainBlock = false;
            $('.loader').each(function () {
                if ($(this).css('display') === 'block') {
                    mainBlock = true;
                }
            });
            if (!mainBlock) {
                $('.main-loader').show();
            }
        }, 2000);
    }).ajaxComplete(function () {
        clearTimeout(timeout)
        $('.main-loader').hide();
        $('[data-toggle="tooltip"]').tooltip();
        $('button').prop('disabled', false);
        $('body').css('pointer-events', 'all');
    }).ajaxError(function (event, request) {
        try {
            let response = JSON.parse(request.responseText);
            let errors = response.errors;
            if (request.status === 402) {
                window.location.reload();
            }
            if (errors) {
                $.each(errors, function (index, value) {
                    showAlert(value, 'danger');
                });
            }
        } catch (e) {
            showAlert('UNDEFINED ERROR', 'danger');
        }
        $('.loader').hide();
    }).ajaxSuccess(function (event, request) {
        try {
            let response = JSON.parse(request.responseText);
            let messages = response.messages;
            if (messages) {
                $.each(messages, function (index, value) {
                    showAlert(value, 'success');
                });
            }
        } catch (e) {
            return false
        }
    });

//------------ALERTS------------
    $('.alert').each(function () {
        var mainAlert = $(this);
        // console.log(mainAlert);
        alertTimeouts(mainAlert);
    });

    $('button[data-dismiss="alert"]').on('click', function (e) {
        $(this).parent().removeClass('alert-open');
        $(this).parent().addClass('alert-close');
    });
//------------END-ALERTS------------
});

function showAlert(message, className) {
    if (!className) {
        className = "info";
    }
    let alert = $('<div class="alert alert-' + className + ' alert-dismissible fade in show" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" aria-hidden="true">&times;</button>' +
         message + '</div>');
    alertTimeouts(alert);
}

function alertTimeouts(alert) {
    $('.alerts').append(alert);
    setTimeout(function () {
        alert.addClass('alert-open');
    }, 0);
    setTimeout(function () {
        alert.removeClass('alert-open');
        alert.addClass('alert-close');
    }, 4000);
    setTimeout(function () {
        alert.hide();
    }, 4600);
}
