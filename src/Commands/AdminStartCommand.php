<?php

namespace Xsoft\AdminPanel;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\File;

class AdminStartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:start {--install}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('install')) {
            if ($this->confirm('Installing "adminPanel" again will reset your routing and all package views. Continue?')) {
                $this->install();
                $this->afterInstall();
            }
        } else {
            $this->update();
            $this->afterUpdate();
        }
    }

    protected function install()
    {
        //Auth
        Artisan::queue('make:auth', ['-q' => '', '--force' => true]);
        echo 'Auth generated' . PHP_EOL;

        //Spatie Permissions
        if (!Schema::hasTable('roles')) {
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "migrations", "--provider" => 'Spatie\Permission\PermissionServiceProvider', '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "config", "--provider" => 'Spatie\Permission\PermissionServiceProvider', '--force' => true]);
        }
        echo 'Spatie Permissions generated' . PHP_EOL;

        //DataTables
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "datatables", '--force' => true]);

        $this->publish();

        $this->delete();
        $this->change();

        echo 'AdminPanel generated' . PHP_EOL;
    }

    private function delete()
    {
        File::delete(resource_path() . '/views/welcome.blade.php');
        File::delete(resource_path() . '/views/home.blade.php');
        File::delete(app_path() . '/Http/Controllers/HomeController.php');
    }

    private function change()
    {
        //routes
        $web = File::get(app_path() . '/../routes/web.php');
        $auth = 'Auth::routes();';
        $newRoutes = File::get(__DIR__ . '\..\..\_copy\routes\web.php');

        $web = str_replace('Route::get(\'/home\', \'HomeController@index\')->name(\'home\');', '', $web);

        $web = str_replace($auth, '', $web);
        $web = $web . $auth;

        $web = str_replace($newRoutes, '', $web);
        $web = $web . PHP_EOL . $newRoutes;

        File::put(app_path() . '/../routes/web.php', $web);
    }

    private function afterInstall()
    {
        Artisan::queue('migrate:refresh', ['-q' => '']);
        echo 'Migrations refreshed' . PHP_EOL;

        Artisan::queue('db:seed', ['-q' => '', '--class' => 'AdminPanelSeeder']);
        echo 'Roles created' . PHP_EOL;
        echo "Superadmin user created. Login in using 'superadmin@example.com - secret'" . PHP_EOL;
    }

    protected function update()
    {
        //AdminPanel
        $this->publish(false);
        echo 'AdminPanel updated' . PHP_EOL;
    }

    private function afterUpdate()
    {

    }

    private function publish($all = true)
    {
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelPublic", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelLang", '--force' => true]);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelSass", '--force' => true]);
        if ($all) {
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelInstall", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelViews", '--force' => true]);
            Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "adminPanelMenuHelper", '--force' => true]);
        }
    }
}
