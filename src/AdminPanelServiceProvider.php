<?php

namespace Xsoft\AdminPanel;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class AdminPanelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../resources/views/' => resource_path() . '/views',
        ], 'adminPanelViews');
        $this->publishes([
            __DIR__ . '/../public/' => public_path(),
        ], 'adminPanelPublic');
        $this->publishes([
            __DIR__ . '/../resources/lang/' => resource_path() . '/lang',
        ], 'adminPanelLang');
        $this->publishes([
            __DIR__ . '/../resources/sass/AdminPanel/' => resource_path() . '/sass/AdminPanel',
        ], 'adminPanelSass');
        $this->publishes([
            __DIR__ . '/../resources/sass/_variables.scss' => resource_path() . '/sass/AdminPanel/_variables.scss',
        ], 'adminPanelSassVariables');
        $this->publishes([
            __DIR__ . '/../src/Controllers/' => app_path() . '/Http/Controllers/',
            __DIR__ . '/../src/Models/' => app_path(),
        ], 'adminPanelInstall');
        if ($this->app->runningInConsole()) {
            $this->commands([
                AdminStartCommand::class,
            ]);
        }
        $this->loadViewsFrom(__DIR__ . '/packages/deleteButton/views', 'deleteButton');

        // Middlewares

        $this->app['router']->aliasMiddleware('role', \Xsoft\AdminPanel\Middlewares\RoleMiddleware::class);
        $this->app['router']->aliasMiddleware('permission', \Xsoft\AdminPanel\Middlewares\PermissionMiddleware::class);
        $this->app['router']->aliasMiddleware('role_or_permission', \Xsoft\AdminPanel\Middlewares\RoleOrPermissionMiddleware::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('deleteButton', function ($route) {
                return "<?php echo Xsoft\\AdminPanel\\DeleteButton::draw($route); ?>";
            });

        });
    }
}
