<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Dane logowania są nieprawidłowe',
    'throttle' => 'Wykryto zbyt wiele prób logowania. Spróbuj ponownie za :seconds sekund.',
    'password' => 'Hasło',
    'confirm_password' => 'Powtórz hasło',
    'login' => 'Logowanie',
    'email' => 'Email',
    'remember_me' => 'Zapamiętaj mnie',
    'confirm_login' => 'Zaloguj',
    'forgot_password' => 'Zapomniałeś hasła?',
    'register' => 'Rejestracja',
    'name' => 'Imię',
    'confirm_register' => 'Zarejestruj',
    'reset_password' => 'Resetuj hasło',
    'reset_send' => 'Wyślij link do resetowania',
    'confirm_reset' => 'Resetuj'
];
