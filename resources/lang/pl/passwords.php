<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi składać się conajmniej z 6 znaków',
    'reset' => 'Twoje hasło zostało zersetowane!',
    'sent' => 'Link do resetowania hasła został wysłany na Twój adre e-mail!',
    'token' => 'Błedny token',
    'user' => "Brak użytkonika o podanym adresie e-mail",

];
