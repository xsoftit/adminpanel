<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Wyloguj',
    'profile' => 'Profil',
    'menu' => [
        'start' => 'Start',
        'config' => 'Config'
    ],
    'lfm' => [
        'choose' => 'Choose files'
    ],
    'dashboard' => [
        'title' => 'Dashboard',
        'subtitle' => 'Administration panel main page'
    ],
    'config' => [
        'title' => 'Config',
        'subtitle' => 'Edit system preferences',
        'alert' => [
            'success' => 'Config saved successful'
        ]
    ]
];
