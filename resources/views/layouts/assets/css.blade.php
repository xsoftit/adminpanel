@section('css')
    <link href="/vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="/vendor/jquery-ui/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="/vendor/jquery-ui/jquery-ui.theme.css" rel="stylesheet">
    <link href="/vendor/fontawesome/css/all.min.css" rel="stylesheet">
    <link href="/vendor/admin-panel/css/adminPanel.css" rel="stylesheet">
@show
