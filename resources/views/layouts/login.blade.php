<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@section('head')
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('page-title')</title>
        @include('layouts.assets.css')
    </head>
@show
@section('body')
    <body class="app login-content rtl pace-done">
    <div class="alerts">@include('flash::message')</div>
    @include('layouts.partials.content')
    @include('layouts.partials.footer')
    @include('layouts.partials.modals')
    @include('layouts.assets.js')
    </body>
@show
</html>
