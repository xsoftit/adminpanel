@extends('layouts.app')

@section('page-title')
    {{__('app.dashboard.title')}}
@endsection

@section('title')
    {{__('app.dashboard.title')}}
@endsection

@section('subtitle')
    {{__('app.dashboard.subtitle')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.dashboard.title')]) !!}
@endsection

@section('page-content')
    Hello, world!
@endsection
