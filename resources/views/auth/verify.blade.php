@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('auth.verify_email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('auth.verify_email_alert') }}
                        </div>
                    @endif

                    {{ __('auth.verify_email_text') }}
                    {{ __('auth.no_link') }}, <a href="{{ route('verification.resend') }}">{{ __('auth.no_link_click') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
